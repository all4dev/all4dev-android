package com.esgi.all4dev.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.lang.reflect.Type;

/**
 * Created by tayeb on 17/06/2016.
 */
public class DateTimeAdapter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    @Override
    public DateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return jsonElement.isJsonNull() ? null : Util.stringToDateTime(jsonElement.getAsString()).withZone(DateTimeZone.getDefault());
    }

    @Override
    public JsonElement serialize(DateTime date, Type type, JsonSerializationContext jsonSerializationContext) {
        return date == null ? new JsonNull() : new JsonPrimitive(Util.dateTimeToString(date));
    }
}

package com.esgi.all4dev.util;

/**
 * Created by tayeb on 08/08/2016.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}

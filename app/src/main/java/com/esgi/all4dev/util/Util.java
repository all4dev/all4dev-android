package com.esgi.all4dev.util;

import android.text.TextUtils;
import android.util.Log;

import com.esgi.all4dev.object.User;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Util {

    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    public static void log(String s) {
        Log.d("All4Dev", s);
    }

    public static final class REFERENCE {
        public static final String AUTH_TOKEN = Config.PACKAGE_NAME + "token";
        public static final String PROJECT = Config.PACKAGE_NAME + "project";
        public static final String USER = Config.PACKAGE_NAME + "user";
        public static final String MEMBER = Config.PACKAGE_NAME + "member";
        public static final String TASK = Config.PACKAGE_NAME + "task";
    }



    public static final class Config {
        public static final String PACKAGE_NAME = "com.esgi.all4dev.";
    }

    public static final String getUserImageProfileUrl(User user) {
        String url;
        if (user.getPhotoPath() == null) {

            return "http://res.all4dev.xyz/users/images/default.png";
        }
        else {
            return "http://res.all4dev.xyz/users/images/" + user.getPhotoPath();
        }
    }

    public static final String dateTimeToString(DateTime dateTime) {
        return dateTimeFormatter.print(dateTime);
    }

    public static final DateTime stringToDateTime(String toDateTime ) {
        return dateTimeFormatter.parseDateTime(toDateTime);
    }

    public static String getDateFormated(DateTime date) {
        int days = Util.diffDay(date);
        if (days == 0)
            return "Aujourd\'hui à " + dateToString("HH:mm", date);
        else if (days == 1)
            return "Hier à " + dateToString("HH:mm", date);
        else if (days < 7) {
            return dateToString("EEEE 'à' HH:mm", date);
        }
        return dateToString("dd MMM yyyy 'à' HH:mm", date);
    }

    public static String dateToString(String format, DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        return date.toString(formatter);
    }

    public static DateTime getNow() {
        return new DateTime().withZone(DateTimeZone.getDefault());
    }

    public static int diffDay(DateTime date) {
        try {
            return Days.daysBetween(date.withZone(DateTimeZone.getDefault()).withTimeAtStartOfDay(), Util.getNow().withTimeAtStartOfDay()).getDays();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}



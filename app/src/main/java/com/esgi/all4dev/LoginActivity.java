package com.esgi.all4dev;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.View;
import android.widget.EditText;

import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.AuthResponse;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_activity_linea_login)
    View linearLogin;

    @BindView(R.id.login_activity_edit_username)
    EditText username;

    @BindView(R.id.login_activity_edit_password)
    EditText password;

    @BindView(R.id.login_activity_progress_login)
    View progressLogin;

    private String authToken;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_activity_btn_login)
    public void login() {
        System.out.println("toto");
        new ApiManager(getApplicationContext()) {

            @Override
            public void onStart() {
                linearLogin.setVisibility(View.INVISIBLE);
                progressLogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess() {
                Intent intent = new Intent(LoginActivity.this, ProjectActivity.class);
                intent.putExtra(Util.REFERENCE.AUTH_TOKEN, authToken);
                intent.putExtra(Util.REFERENCE.USER, user);
                startActivity(intent);
            }

            @Override
            public void onError(AppException e) {
                e.toast(getApplicationContext());
                linearLogin.setVisibility(View.VISIBLE);
                progressLogin.setVisibility(View.INVISIBLE);
            }

            @Override
            public void always() {

            }
        }.execute(
                new ApiRequest<AuthResponse>() {

                    @Override
                    public Call<AuthResponse> call(ApiService service) {
                        return service.auth(username.getText().toString(), password.getText().toString());
                    }

                    @Override
                    public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                        authToken = "Bearer " + response.body().getKey();
                        Util.log("Auth Ok");
                        Util.log(authToken);

                    }
                },
                new ApiRequest<User>() {
                    @Override
                    public Call<User> call(ApiService service) {
                        return service.getMe(authToken);
                    }

                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        user = response.body();
                    }
                }
        );
    }
}

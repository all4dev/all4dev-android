package com.esgi.all4dev;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.esgi.all4dev.adapter.ProjectsAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.notification.RegistrationIntentService;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.QuickstartPreferences;
import com.esgi.all4dev.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by tayeb on 01/06/2016.
 */
public class ProjectActivity extends AppCompatActivity implements ProjectsAdapter.ProjectClickListener {

    @BindView(R.id.activity_project_swipe_container)
    SwipeRefreshLayout swipe;


    @BindView(R.id.activity_project_list_projects)
    RecyclerView projects_recycler_view;

    @BindView(R.id.login_activity_text_bienvenue)
    TextView login_activity_text_bienvenue;

    @BindView(R.id.login_activity_progress_login)
    View progressLogin;

    //GCM Notif
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "ProjectActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    private ProjectsAdapter adapter;
    private List<Project> projects;

    private String authToken;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        ButterKnife.bind(this);
        authToken = getIntent().getStringExtra(Util.REFERENCE.AUTH_TOKEN);
        user = getIntent().getParcelableExtra(Util.REFERENCE.USER);

        projects_recycler_view.setHasFixedSize(true);
        projects_recycler_view.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        projects_recycler_view.setLayoutManager(new LinearLayoutManager(ProjectActivity.this, LinearLayoutManager.VERTICAL, false));

        adapter = new ProjectsAdapter(this);
        projects_recycler_view.setAdapter(adapter);
        pullProjects();
        login_activity_text_bienvenue.setText("Bonjour, " + user.getUserName());

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullProjects();
            }
        });

        //GCM Notif
        //mRegistrationProgressBar = (ProgressBar) findViewById(R.id.registrationProgressBar);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    //mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                    //mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };

        if (checkPlayServices()) {
            //Start IntentService to register this application with GCM.
            Util.log("Service gcm ok");
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * Vérifier si notre utilisateur a l'application Google Play Service
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(int position) {
        final Project project = projects.get(position);
        new ApiManager(getApplicationContext()) {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess() {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra(Util.REFERENCE.AUTH_TOKEN, authToken);
                intent.putExtra(Util.REFERENCE.USER, user);
                intent.putExtra(Util.REFERENCE.PROJECT, project);
                Util.log(project.toString());
                startActivity(intent);
            }

            @Override
            public void onError(AppException e) {

            }

            @Override
            public void always() {

            }
        }.execute(new ApiRequest<List<Member>>() {
            @Override
            public Call<List<Member>> call(ApiService service) {
                return service.getProjectsMembers(authToken, project.getLeaderUsername(), project.getName());
            }

            @Override
            public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                project.setMembers(response.body());
            }
        });
    }


    public void pullProjects() {

        new ApiManager(getApplicationContext()) {

            @Override
            public void onStart() {
                swipe.setRefreshing(true);
            }

            @Override
            public void onSuccess() {
                adapter.clear();
                for (int i = 0; i < projects.size(); i++) {
                    Project project = projects.get(i);
                    adapter.addProject(project);
                    Util.log(project.getId() + "    " + project.getName()+ "    "+project.getLeaderUsername());
                }
            }

            @Override
            public void onError(AppException e) {
                progressLogin.setVisibility(View.VISIBLE);
                e.toast(getApplicationContext());
            }

            @Override
            public void always() {
                swipe.setRefreshing(false);
            }
        }.execute(new ApiRequest<List<Project>>() {
            @Override
            public Call<List<Project>> call(ApiService service) {
                return service.getMeProjects(authToken);
            }

            @Override
            public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                if (response.isSuccessful()) {
                    projects = response.body();
                }

            }
        });

    }
}

package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;


public class Task implements Parcelable {
    private long id;
    private String name;
    private String body;

    private String type;
    private String status;

    @SerializedName("author_id")
    private long authorId;

    @SerializedName("author_name")
    private String authorName;

    @SerializedName("last_update")
    private DateTime lastUpdate;

    //private transient List<Commit> commits;

    //private transient List<Comment> comments;

    @SerializedName("branchName")
    private String branchName;

    public Task() {
        super();
    }

    protected Task(Parcel in) {
        id = in.readLong();
        name = in.readString();
        body = in.readString();
        type = in.readString();
        status = in.readString();
        authorId = in.readLong();
        authorName = in.readString();
        branchName = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    public String getBranchName() {
        return branchName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(body);
        dest.writeString(type);
        dest.writeString(status);
        dest.writeLong(authorId);
        dest.writeString(authorName);
        dest.writeString(branchName);
    }
}

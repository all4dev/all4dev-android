package com.esgi.all4dev.object;


import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;

public class Version implements Parcelable{
    private long id;
    private String version;

    public long getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.version);
    }

    protected Version(Parcel in) {
        this.id = in.readLong();
        this.version = in.readString();
    }

    public static final Creator<Version> CREATOR = new Creator<Version>() {
        @Override
        public Version createFromParcel(Parcel source) {
            return new Version(source);
        }

        @Override
        public Version[] newArray(int size) {
            return new Version[size];
        }
    };
}

package com.esgi.all4dev.object;

import org.joda.time.DateTime;

public interface DateTimeable {
    DateTime getDateTime();
}

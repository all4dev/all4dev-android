package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

/**
 * Created by tayeb on 21/06/2016.
 */
public class Commit implements Parcelable, DateTimeable {

    private String hash;
    private String subject;

    @SerializedName("author_name")
    private String authorName;

    @SerializedName("author_email")
    private String authorEmail;
    private DateTime date;
    private String timezone;

    @SerializedName("push_uuid")
    private String pushUUID;

    public String getHash() {
        return hash;
    }

    public String getSubject() {
        return subject;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public DateTime getDate() {
        return date;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getPushUUID() {
        return pushUUID;
    }

    @Override
    public DateTime getDateTime() {
        return getDate();
    }

    @Override
    public String toString() {
        return "Commit : " + subject + "\n" + "Auteur : " +  authorName + "\n" + "Date : " + date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.hash);
        dest.writeString(this.subject);
        dest.writeString(this.authorName);
        dest.writeString(this.authorEmail);
        dest.writeString(Util.dateTimeToString(this.date));
        dest.writeString(this.timezone);
        dest.writeString(this.pushUUID);
    }

    public Commit() {
    }

    protected Commit(Parcel in) {
        this.hash = in.readString();
        this.subject = in.readString();
        this.authorName = in.readString();
        this.authorEmail = in.readString();
        this.date = Util.stringToDateTime(in.readString());
        this.timezone = in.readString();
        this.pushUUID = in.readString();
    }

    public static final Creator<Commit> CREATOR = new Creator<Commit>() {
        @Override
        public Commit createFromParcel(Parcel source) {
            return new Commit(source);
        }

        @Override
        public Commit[] newArray(int size) {
            return new Commit[size];
        }
    };
}

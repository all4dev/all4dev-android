package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

/**
 * Created by tayeb on 21/06/2016.
 */
public class Comment implements Parcelable, DateTimeable {

    private long id;
    private String body;

    @SerializedName("creation_date")
    private DateTime creationDate;

    @SerializedName("author_id")
    private long authorId;

    @SerializedName("author_name")
    private String authorName;

    public long getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    @Override
    public DateTime getDateTime() {
        return getCreationDate();
    }

    @Override
    public String toString() {
        return "Commentaire : " + body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.body);
        dest.writeString(Util.dateTimeToString(this.creationDate));
        dest.writeLong(this.authorId);
        dest.writeString(this.authorName);
    }

    public Comment() {
    }

    protected Comment(Parcel in) {
        this.id = in.readLong();
        this.body = in.readString();
        this.creationDate = Util.stringToDateTime(in.readString());
        this.authorId = in.readLong();
        this.authorName = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}

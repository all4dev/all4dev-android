package com.esgi.all4dev.object;

import com.google.gson.annotations.SerializedName;

public class AuthResponse {

    @SerializedName("Authorization")
    private String key;

    public String getKey() {
        return key;
    }
}

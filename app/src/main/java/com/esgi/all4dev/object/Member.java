package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

public class Member extends User implements Parcelable {

    private Member.Access access;

    @SerializedName("add_date")
    private DateTime addDate;

    @SerializedName("remove_date")
    private DateTime removeDate;

    public Member.Access getAccess() {
        return access;
    }

    public void setAccess(Member.Access access) {
        this.access = access;
    }

    public DateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(DateTime addDate) {
        this.addDate = addDate;
    }

    public DateTime getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(DateTime removeDate) {
        this.removeDate = removeDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.access.ordinal());
        dest.writeString(Util.dateTimeToString(this.addDate));
        dest.writeString(Util.dateTimeToString(this.removeDate));
    }

    public Member() {
    }

    protected Member(Parcel in) {
        super(in);
        this.access = Member.Access.values()[in.readInt()];
        this.addDate =  Util.stringToDateTime(in.readString());
        this.removeDate =  Util.stringToDateTime(in.readString());
    }

    public static final Creator<Member> CREATOR = new Creator<Member>() {
        @Override
        public Member createFromParcel(Parcel source) {
            return new Member(source);
        }

        @Override
        public Member[] newArray(int size) {
            return new Member[size];
        }
    };

    public enum Access {
        ADMIN, MOD, DEV, VIEWER;

        @Override
        public String toString() {
            String name = super.toString();
            return Character.toUpperCase(name.charAt(0)) + name.substring(1);
        }
    }
}

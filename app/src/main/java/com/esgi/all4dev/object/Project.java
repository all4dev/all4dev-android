package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Project implements Parcelable {
    private long id;
    private String name;
    private String description;
    private String repo_name;
    private String version;


    @SerializedName("leader_id")
    private long leaderId;

    @SerializedName("leader_username")
    private String leaderUsername;

    @SerializedName("last_update")
    private DateTime lastUpdate;

    private transient List<Member> members;

    public long getId() {
        return id;
    }

    public Project(String description, long id, String name) {
        this.description = description;
        this.id = id;
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(long leaderId) {
        this.leaderId = leaderId;
    }

    public String getLeaderUsername() {
        return leaderUsername;
    }

    public void setLeaderUsername(String leaderUsername) {
        this.leaderUsername = leaderUsername;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public void setCurrentVersion(String version) {
        this.version = version;
    }

    public String getCurrentVersion() {
        return version;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.version);
        dest.writeString(this.repo_name);
        dest.writeLong(this.leaderId);
        dest.writeString(this.leaderUsername);
        dest.writeString(Util.dateTimeToString(this.lastUpdate));
        dest.writeList(members);
    }

    protected Project(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
        this.version = in.readString();
        this.repo_name = in.readString();
        this.leaderId = in.readLong();
        this.leaderUsername = in.readString();
        this.lastUpdate = Util.stringToDateTime(in.readString());
        this.members = new ArrayList<>();
        in.readList(this.members, Member.class.getClassLoader());
    }

    public static final Parcelable.Creator<Project> CREATOR = new Parcelable.Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel source) {
            return new Project(source);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };
}
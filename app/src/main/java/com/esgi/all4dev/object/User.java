package com.esgi.all4dev.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.esgi.all4dev.util.Util;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

public class User implements Parcelable {

    private long id;
    private String username;
    private String firstname;
    private String lastname;
    private String email;

    @SerializedName("photo_path")
    private String photoPath;

    @SerializedName("creation_date")
    private DateTime creationDate;


    public User() {}


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String lastname) {
        this.username = username;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.username);
        dest.writeString(this.firstname);
        dest.writeString(this.lastname);
        dest.writeString(this.email);
        dest.writeString(this.photoPath);
        dest.writeString(Util.dateTimeToString(this.creationDate));
    }

    protected User(Parcel in) {
        this.id = in.readLong();
        this.username = in.readString();
        this.firstname = in.readString();
        this.lastname = in.readString();
        this.email = in.readString();
        this.photoPath = in.readString();
        this.creationDate = Util.stringToDateTime(in.readString());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}

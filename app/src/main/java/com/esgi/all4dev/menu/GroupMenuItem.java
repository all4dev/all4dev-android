package com.esgi.all4dev.menu;



import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class GroupMenuItem extends MenuItem {

    private List<MenuItem> items;

    public GroupMenuItem(int icon, String name, Fragment fragment) {
        super(icon, name, fragment);
        items = new ArrayList<MenuItem>();
    }

    public void addItem(MenuItem item) {
        items.add(item);
    }

    public void addItems(MenuItem... items) {
        for (MenuItem item: items)
            addItem(item);
    }

    public List<MenuItem> getItems() {
        return items;
    }
}

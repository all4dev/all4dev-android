package com.esgi.all4dev.menu;


import android.support.v4.app.Fragment;

public class MenuItem {
    private int icon;
    private String name;
    private Fragment fragment;

    public MenuItem(String name, Fragment fragment) {
        this(-1, name, fragment);
    }

    public MenuItem(int icon, String name, Fragment fragment) {
        this.icon = icon;
        this.name = name;
        this.fragment = fragment;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean hasIcon() {
        return icon != -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public boolean isSelectable() {
        return getFragment() != null;
    }
}

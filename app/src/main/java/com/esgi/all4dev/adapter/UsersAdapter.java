package com.esgi.all4dev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.esgi.all4dev.R;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tayeb on 03/06/2016.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersHolder> {

    private List<User> users;
    private Context context;
    private final UsersAdapter.ClickListener listener;

    public UsersAdapter(UsersAdapter.ClickListener listener, Context context) {
        users = new ArrayList<>();
        this.listener = listener;
        this.context = context;
    }

    @Override
    public UsersAdapter.UsersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_users, parent, false);
        return new UsersAdapter.UsersHolder(row);
    }

    @Override
    public void onBindViewHolder(UsersAdapter.UsersHolder holder, int position) {

        User user = users.get(position);
        holder.username.setText(user.getUserName());
        holder.firstName.setText(user.getFirstName());
        holder.lastName.setText(user.getLastName());
        Glide.with(context)
                .load(Util.getUserImageProfileUrl(user))
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setUsers(List<User> users) {
        this.users.clear();
        this.users.addAll(users);
        notifyDataSetChanged();
    }

    public void clear() {
        users.clear();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        users.remove(position);
        notifyItemRemoved(position);
    }

    public User getSelectedMember(int position) {
        return users.get(position);
    }

    public class UsersHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        @BindView(R.id.users_adapter_image)
        ImageView image;

        @BindView(R.id.users_adapter_text_username)
        TextView username;

        @BindView(R.id.users_adapter_text_firstname)
        TextView firstName;

        @BindView(R.id.users_adapter_text_lastname)
        TextView lastName;

        public UsersHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }


        @Override
        public void onClick(View v) {
            listener.onClick(getLayoutPosition());
        }
    }
    public interface ClickListener {
        void onClick(int position);
    }

}

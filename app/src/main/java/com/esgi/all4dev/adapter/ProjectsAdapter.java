package com.esgi.all4dev.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Project;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tayeb on 30/05/2016.
 */
public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ProjectHolder>  {

    private List<Project> projects;
    private final ProjectClickListener listener;

    public ProjectsAdapter(ProjectClickListener listener) {
        projects = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public ProjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_projects, parent, false);

        return new ProjectHolder(row);
    }

    @Override
    public void onBindViewHolder(ProjectHolder holder, int position) {
        Project currentProject = projects.get(position);

        holder.name.setText(currentProject.getName());
        holder.desc.setText(currentProject.getDescription());
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    public void addProject(Project project) {
        projects.add(project);
        notifyDataSetChanged();
    }

    public void setProjects(List<Project> projects) {
        projects.clear();
        projects.addAll(projects);
        notifyDataSetChanged();
    }

    /* Within the RecyclerView.Adapter class */

    // Clean all elements of the recycler
    public void clear() {
        projects.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Project> project) {
        projects.addAll(project);
        notifyDataSetChanged();
    }

    public Project getSelectedProject(int position) {
        return projects.get(position);
    }

    public class ProjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        @BindView(R.id.projects_adapter_text_name)
        TextView name;

        @BindView(R.id.projects_adapter_text_desc)
        TextView desc;

        public ProjectHolder(View itemView){

            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getLayoutPosition());

        }

    }
    public interface ProjectClickListener {
        void onClick(int position);
    }
}

package com.esgi.all4dev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.object.Message;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.esgi.all4dev.R;

import java.util.ArrayList;

/**
 * Created by tayeb on 07/06/2016.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder> {

    private ArrayList<Message> messages;

    private static final int RIGHT = 0, LEFT = 1;
    private Context context;


    public MessagesAdapter(ArrayList<Message> messages, Context context) {
        this.messages = messages;
        this.context = context;
    }


    /**
     *  On utilise cette méthode pour pouvoir afficher après la "bonne" vue
     *  Si c'est notre message alors cela sera "RIGHT"
     *  et si ce sont les messages des autres alors "LEFT"
     */
    @Override
    public int getItemViewType(int position) {

        if(messages.get(position).isMe())
            return RIGHT;
        else
            return LEFT;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        switch (viewType) {

            case RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_message_right, parent,false);
                break;
            case LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_message_left, parent,false);
                break;
        }

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        String format = "dd/MM/yy à H:mm:ss";
        String dateFrancaise = "";

        java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
        java.util.Date date = new java.util.Date();
        dateFrancaise = formater.format(date);

        Message message = messages.get(position);
        holder.txtName.setText(message.getName());
        holder.txtMessage.setText(message.getMessage());
        holder.txtDate.setText(dateFrancaise);

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.name)
        TextView txtName;

        @BindView(R.id.message)
        TextView txtMessage;

        @BindView(R.id.date)
        TextView txtDate;

        public MessageViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }
}

package com.esgi.all4dev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tayeb on 03/06/2016.
 */
public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MemberHolder> {

    private List<Member> members;
    private Context context;
    private final MemberClickListener listener;

    public MembersAdapter(MemberClickListener listener, Context context) {
        members = new ArrayList<>();
        this.listener = listener;
        this.context = context;
    }

    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_members, parent, false);

        return new MemberHolder(row);
    }

    @Override
    public void onBindViewHolder(MembersAdapter.MemberHolder holder, int position) {

        Member currentMember = members.get(position);

        holder.firstName.setText(currentMember.getFirstName());
        holder.lastName.setText(currentMember.getLastName());
        holder.statut.setText(currentMember.getAccess().toString());
        Glide.with(context)
                .load(Util.getUserImageProfileUrl(currentMember))
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    public void setMembers(List<Member> members) {
        this.members.clear();
        this.members.addAll(members);
        notifyDataSetChanged();
    }

    public void clear() {
        members.clear();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        members.remove(position);
        notifyItemRemoved(position);
    }

    public Member getSelectedMember(int position) {
        return members.get(position);
    }

    public class MemberHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        @BindView(R.id.members_adapter_text_firstname)
        TextView firstName;

        @BindView(R.id.members_adapter_text_lastname)
        TextView lastName;

        @BindView(R.id.members_adapter_image)
        ImageView image;

        @BindView(R.id.members_adapter_statut)
        TextView statut;

        public MemberHolder(View itemView){

            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }


        @Override
        public void onClick(View v) {
            listener.onClick(getLayoutPosition());
        }
    }
    public interface MemberClickListener {
        void onClick(int position);
    }

}

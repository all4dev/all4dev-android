package com.esgi.all4dev.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esgi.all4dev.R;
import com.esgi.all4dev.menu.GroupMenuItem;
import com.esgi.all4dev.menu.MenuItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends BaseExpandableListAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<GroupMenuItem> items;

    public MenuAdapter(Context context, List<GroupMenuItem> items) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getGroupCount() {
        return items.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return items.get(groupPosition).getItems().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return items.get(groupPosition).getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition*10;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition*10+childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupMenuViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_menu_group, null);
            viewHolder = new GroupMenuViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupMenuViewHolder) convertView.getTag();
        }
        GroupMenuItem group = items.get(groupPosition);
        viewHolder.icon.setImageResource(group.getIcon());
        viewHolder.name.setText(group.getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MenuViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_menu_child, null);
            viewHolder = new MenuViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MenuViewHolder) convertView.getTag();
        }
        MenuItem group = items.get(groupPosition).getItems().get(childPosition);
        viewHolder.icon.setImageResource(group.getIcon());
        viewHolder.name.setText(group.getName());

        return convertView;
    }


    protected class GroupMenuViewHolder {
        @BindView(R.id.menu_group_adapter_image_icon)
        ImageView icon;

        @BindView(R.id.menu_group_adapter_text_name)
        TextView name;

        public GroupMenuViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    protected class MenuViewHolder {
        @BindView(R.id.menu_group_adapter_image_icon)
        ImageView icon;

        @BindView(R.id.menu_group_adapter_text_name)
        TextView name;

        public MenuViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

package com.esgi.all4dev.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Comment;
import com.esgi.all4dev.object.Commit;
import com.esgi.all4dev.object.DateTimeable;
import com.esgi.all4dev.util.Util;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskAdapter extends BaseAdapter {

    private static final int TYPE_COMMIT = 0;
    private static final int TYPE_COMMENT = 1;
    private static final int TYPE_MAX_COUNT = 2;

    private ArrayList<DateTimeable> dateTimeables = new ArrayList<DateTimeable>();
    private ArrayList<Row> rows;
    private LayoutInflater mInflater;
    Context context;

    private TreeSet<Integer> mSeparatorsSet = new TreeSet<Integer>();

    public TaskAdapter(Context context, ArrayList dateTimeables) {
        this.dateTimeables = dateTimeables;
        this.context = context;
        mInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getItemViewType(int position) {
        return (dateTimeables.get(position) instanceof Comment) ? TYPE_COMMENT : TYPE_COMMIT;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getCount() {
        return dateTimeables.size();
    }

    @Override
    public DateTimeable getItem(int position) {
        return dateTimeables.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public void clear() {
        dateTimeables.clear();
        notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Row row = null;
        if (convertView == null) {
            int type = getItemViewType(position);
            if (type == TYPE_COMMENT) {
                row = new CommentRow();
            } else if (type == TYPE_COMMIT) {
                row = new CommitRow();
            }
            if (row == null)
                return null;
            convertView = row.generateView(mInflater);
            convertView.setTag(row);
        } else {
            row = (Row) convertView.getTag();
        }
        row.bind(dateTimeables.get(position));
        return convertView;
    }

    public static class ViewHolder {
        public TextView textView;
    }

    public abstract class Row<T> {

        public View generateView(LayoutInflater layoutInflater) {
            View view = layoutInflater.inflate(getLayout(), null);
            ButterKnife.bind(this, view);
            return view;
        }

        public abstract int getLayout();
        public abstract int getViewType();
        public abstract void bind(T t);
    }

    public class CommitRow extends Row<Commit> {

        @BindView(R.id.commit_adapter_text_author)
        TextView author;

        @BindView(R.id.commit_adapter_text_date)
        TextView date;

        @BindView(R.id.commit_adapter_text_body)
        TextView body;

        @Override
        public int getViewType() {
            return TYPE_COMMIT;
        }

        @Override
        public int getLayout() {
            return R.layout.adapter_commits;
        }

        public void bind(Commit commit) {
            author.setText(commit.getAuthorName());
            date.setText(Util.getDateFormated(commit.getDate()));
            body.setText(commit.getSubject());
        }
    }

    public class CommentRow extends Row<Comment> {

        @BindView(R.id.comment_adapter_text_author)
        TextView author;

        @BindView(R.id.comment_adapter_text_date)
        TextView date;

        @BindView(R.id.comment_adapter_text_body)
        TextView body;

        @Override
        public int getViewType() {
            return TYPE_COMMENT;
        }

        @Override
        public int getLayout() {
            return R.layout.adapter_comments;
        }

        public void bind(Comment comment) {
            author.setText(comment.getAuthorName());
            date.setText(Util.getDateFormated(comment.getCreationDate()));
            body.setText(comment.getBody());
        }
    }
}

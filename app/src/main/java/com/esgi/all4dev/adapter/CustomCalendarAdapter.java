package com.esgi.all4dev.adapter;

import com.esgi.all4dev.object.CustomDayCell;
import com.telerik.widget.calendar.CalendarAdapter;
import com.telerik.widget.calendar.CalendarDayCell;
import com.telerik.widget.calendar.RadCalendarView;

/**
 * Created by tayeb on 17/06/2016.
 */
public class CustomCalendarAdapter extends CalendarAdapter {


    //Adapter permettant de rajouter un point sur le jour où il y aura un évenement
    public CustomCalendarAdapter(RadCalendarView owner) {
        super(owner);
    }


    @Override
    public CalendarDayCell getDateCell() {
        CustomDayCell cell = new CustomDayCell(owner);

        this.dateCells.add(cell);

        return cell;
    }


}

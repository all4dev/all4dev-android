package com.esgi.all4dev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Task;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Keena on 29/05/16.
 */
public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TaskHolder> {

    private List<Task> tasks;
    private Context context;
    private final TaskClickListener listener;
    private final TaskLongClickListener onLongListener;

    public TasksAdapter(TaskClickListener listener, TaskLongClickListener onLongListener, Context context) {
        tasks = new ArrayList<>();
        this.listener = listener;
        this.onLongListener = onLongListener;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tasks, parent, false);
        return new TaskHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        Task currentTask = tasks.get(position);
        holder.bind(tasks.get(position));
    }

    public void addTask(Task task) {
        tasks.add(task);
        notifyDataSetChanged();
    }

    public void clear() {
        tasks.clear();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        tasks.remove(position);
        notifyItemRemoved(position);
    }

    public Task getSelectedTask(int position) {
        return tasks.get(position);
    }

    protected class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @BindView(R.id.tasks_adapter_text_title)
        TextView title;

        @BindView(R.id.tasks_adapter_text_desc)
        TextView desc;

        @BindView(R.id.tasks_adapter_text_author)
        TextView author;

        public TaskHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Task task){
            title.setText(task.getName());
            desc.setText(task.getBody());
            author.setText("Créer par : " + task.getAuthorName() + " ");
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getLayoutPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            onLongListener.onLongClick(getLayoutPosition());
            return true;
        }
    }

    public interface TaskClickListener {
        void onClick(int position);
    }

    public interface TaskLongClickListener {
        void onLongClick(int position);
    }



}

package com.esgi.all4dev.api.exception;


import com.esgi.all4dev.R;

public class NotFoundException extends AppException {
    private static final long serialVersionUID = 1L;

    public NotFoundException() {
        super(R.string.exception_default);
    }
}

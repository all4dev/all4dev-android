package com.esgi.all4dev.api.exception;

import com.esgi.all4dev.R;

public class NetworkException extends AppException {
    private static final long serialVersionUID = 1L;


    public NetworkException() {
        super(R.string.exception_network);
    }
}

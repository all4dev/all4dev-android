package com.esgi.all4dev.api.exception;

import android.content.Context;
import android.widget.Toast;

import com.esgi.all4dev.R;

public class AppException extends Exception {
    private int resMsg = -1;

    public AppException() {
        this(R.string.exception_default);
    }

    public AppException(int resMsg) {
        this.resMsg = resMsg;
    }

    public int getMsg() {
        return resMsg;
    }

    public void toast(Context context) {
        Toast.makeText(context, getMsg(), Toast.LENGTH_SHORT).show();
    }
}

package com.esgi.all4dev.api.exception;

public class CanceledException extends AppException {
    private static final long serialVersionUID = 1L;

    public CanceledException() {
        super(-1);
    }
}

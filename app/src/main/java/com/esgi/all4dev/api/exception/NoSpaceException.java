package com.esgi.all4dev.api.exception;

import com.esgi.all4dev.R;

public class NoSpaceException extends AppException {
    private static final long serialVersionUID = 1L;


    public NoSpaceException() {
        super(R.string.exception_no_space);
    }
}

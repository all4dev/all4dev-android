package com.esgi.all4dev.api;

import com.esgi.all4dev.object.AuthResponse;
import com.esgi.all4dev.object.Comment;
import com.esgi.all4dev.object.Commit;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.Task;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.object.Version;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    //Prod
    String BASE_URL = "http://api.all4dev.xyz/";

    @POST("auth")
    @FormUrlEncoded
    Call<AuthResponse> auth(@Field("username") String username, @Field("password") String password);


    @GET("me")
    Call<User> getMe(@Header("Authorization") String authorization);

    @GET("me/projects")
    Call<List<Project>> getMeProjects(@Header("Authorization") String authorization);

    @GET("users/{username}/projects")
    Call<List<Project>> getProjects(@Header("Authorization") String authorization, @Path("username") String username);
    @GET("users/{username}/projects/{projectname}/versions")
    Call<List<Version>> getProjectVersions(@Header("Authorization") String authorization,
                                           @Path("username") String username,
                                           @Path("projectname") String projectName);

    @GET("users")
    Call<List<User>> getUsers(
            @Header("Authorization") String authorization,
            @Query("username") String username,
            @Query("email") String email
    );

    @GET("users/{username}")
    Call<User> getUsers(@Header("Authorization") String authorization, @Path("username") String username);

    @FormUrlEncoded
    @PUT("users/{username}")
    Call<User> putUsers(
            @Header("Authorization") String authorization,
            @Path("username") String username,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("email") String email
    );

    @GET("/users/{username}/projects/{projectName}/members")
    Call<List<Member>> getProjectsMembers(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectName") String projectName);

    @DELETE("/users/{username}/projects/{projectName}/members/{memberUsername}")
    Call<Void> deleteProjectMember(@Header("Authorization") String authorization,
                                     @Path("username") String username,
                                     @Path("projectName") String projectName,
                                     @Path("memberUsername") String memberName);

    @GET("/users/{username}/projects/{projectName}/tasks")
    Call<List<Task>> getTasks(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectName") String projectName);

    @GET("/users/{username}/projects/{projectName}/tasks")
    Call<Task> getTask(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectName") String projectName);

    @POST("/users/{username}/projects/{projectName}/members")
    @FormUrlEncoded
    Call<Member> addMember(@Header("Authorization") String authorization,
                           @Path("username") String username,
                           @Path("projectName") String projectName,
                           @Field("username") String memberUsername,
                           @Field("access") String access);


    @GET("/users/{username}/projects/{projectName}/tasks/{taskId}/commits")
    Call<List<Commit>> getCommits(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectName") String projectName, @Path("taskId") long taskId);

    @GET("/users/{username}/projects/{projectName}/tasks/{taskId}/comments")
    Call<List<Comment>> getComments(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectName") String projectName, @Path("taskId") long taskId);

    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/tasks/{task_id}/comments")
    Call<Comment> addTaskComment(@Header("Authorization") String authorization, @Path("username") String username, @Path("projectname") String projectName, @Path("task_id") long taskId, @Field("body") String body);

}

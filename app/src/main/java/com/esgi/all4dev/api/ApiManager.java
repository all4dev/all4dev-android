package com.esgi.all4dev.api;

import android.content.Context;
import android.os.AsyncTask;

import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.api.exception.CanceledException;
import com.esgi.all4dev.api.exception.NetworkException;
import com.esgi.all4dev.object.EnumAdapter;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.util.DateTimeAdapter;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class ApiManager extends AsyncTask<ApiRequest, Integer, Void> {
    protected Context context;
    private Exception error = null;
    private boolean cancel;

    public ApiManager(Context context) {
        this.context = context;
    }

    public abstract void onStart();

    public abstract void onSuccess();

    public abstract void onError(AppException e);

    public abstract void always();

    @Override
    protected Void doInBackground(ApiRequest... params) {
        ApiService apiService = new Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder()
                                .registerTypeAdapter(DateTime.class, new DateTimeAdapter())
                                .registerTypeAdapter(Member.Access.class, new EnumAdapter<Member.Access>(Member.Access.class))
                                .create())
                )
                .build()
                .create(ApiService.class);

        for (ApiRequest request : params) {
            try {
                Call call = request.getCall(apiService);
                Response response = call.execute();
                if (response.isSuccessful()) {
                    request.onResponse(call, response);
                } else { //TODO: Throw the good Exception
                    System.out.println(response.errorBody().string());
                    System.out.println(response.code());
                    error = new AppException();
                    break;
                }
            } catch (IOException e) {
                error = new NetworkException();
                break;
            }
        }
        return null;
    }

    @Override
    public void onPreExecute() {
        onStart();
    }

    @Override
    public void onPostExecute(Void result) {
        if (isCancel())
            error = new CanceledException();
        if (error != null)
            onError(((error instanceof AppException)) ? (AppException) error : new AppException());
        else
            onSuccess();
        always();
    }

    public void sendProgress(int progress) {
        publishProgress(progress);
    }

    public void cancel() {
        this.cancel = true;
    }

    public boolean isCancel() {
        return cancel;
    }
}
package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.TasksAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.Task;
import com.esgi.all4dev.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class TasksFragment extends Fragment implements TasksAdapter.TaskClickListener, TasksAdapter.TaskLongClickListener  {
    @BindView(R.id.tasks_fragment_recycler_view)
    RecyclerView tasks_recycler_view;

    @BindView(R.id.fragment_task_swipe_container)
    SwipeRefreshLayout swipe;

    @BindView(R.id.fragment_task_text)
    TextView text;

    private List<Task> tasks;
    MainActivity mainActivity;
    TasksAdapter adapter;
    Project project;
    private String authToken;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tasksViews = inflater.inflate(R.layout.fragment_tasks, container, false);
        ButterKnife.bind(this, tasksViews);

        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        project = mainActivity.getProject();

        tasks_recycler_view.setHasFixedSize(true);
        tasks_recycler_view.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        tasks_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new TasksAdapter(this, this, getContext());
        tasks_recycler_view.setAdapter(adapter);

        text.setText("Tache du projet " + project.getName());


        pullTasks();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullTasks();
            }
        });

        //Rattache le swipe de la classe TaskTouch au recycler view
        ItemTouchHelper.Callback callback = new TaskTouch(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(tasks_recycler_view);

        return tasksViews;
    }

    public void pullTasks() {
        new ApiManager(getActivity()) {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess() {
                adapter.clear();
                for (int i = 0; i < tasks.size(); i++) {
                    Task task = tasks.get(i);
                    adapter.addTask(task);
                    Util.log(task.getAuthorName() + " " + getStatus() + " " + task.getName() + " " + task.getId() + "  " + task.getAuthorId());
                }
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {
                swipe.setRefreshing(false);

            }
        }.execute(
                new ApiRequest<List<Task>>() {

                    @Override
                    public Call<List<Task>> call(ApiService service) {
                        return service.getTasks(authToken, project.getLeaderUsername(), project.getName());
                    }

                    @Override
                    public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                        tasks = response.body();
                    }
                });
    }

    @Override
    public void onClick(int position) {
        Task selectedTask = tasks.get(position);
        Util.log(selectedTask.toString());
        mainActivity.popFragment(selectedTask.getName(), TaskFragment.newInstance(selectedTask));
    }

    @Override
    public void onLongClick(int position) {
        Task selectedTask = tasks.get(position);
        mainActivity.popFragment(selectedTask.getName(), TasksUpdateFragment.newInstance(selectedTask));
    }

    //Class qui permet de supprimer un élément lors du swipe
    public class TaskTouch extends ItemTouchHelper.SimpleCallback {
        private TasksAdapter tasksAdapter;

        public TaskTouch(TasksAdapter tasksAdapter){
            super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            this.tasksAdapter = tasksAdapter;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            //TODO: Not implemented here
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            tasksAdapter.remove(viewHolder.getAdapterPosition());
        }
    }
}

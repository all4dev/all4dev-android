package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    @BindView(R.id.fragment_add_member_image)
    ImageView userimage;

    @BindView(R.id.fragment_add_member_username)
    TextView username;

    @BindView(R.id.fragment_add_member_firstname)
    EditText userfirstname;

    @BindView(R.id.fragment_add_member_lastname)
    EditText userlastname;

    @BindView(R.id.fragment_add_member_email)
    EditText useremail;

    MainActivity mainActivity;

    User user;

    boolean canEdit = false;

    public static ProfileFragment newInstance(User user) {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        args.putParcelable(Util.REFERENCE.USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        mainActivity = (MainActivity) getActivity();
        user = getArguments().getParcelable(Util.REFERENCE.USER);

        invalidate();
        return rootView;
    }

    public void invalidate() {
        userfirstname.setText(user.getFirstName());
        userlastname.setText(user.getLastName());
        useremail.setText(user.getEmail());
        Glide.with(getActivity())
                .load(Util.getUserImageProfileUrl(user))
                .into(userimage);

    }

    @OnClick(R.id.fragment_add_member_btn_save)
        public void save() {
        if (!canEdit) {
            new ApiManager(getActivity()) {

                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess() {
                    Toast.makeText(getActivity(), "Enregistrement effectué avec succeès", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(AppException e) {
                    e.toast(getActivity());
                    Util.log("fail");

                }

                @Override
                public void always() {
                    invalidate();
                }
            }.execute(new ApiRequest<User>() {
                @Override
                public Call<User> call(ApiService service) {
                    return service.putUsers(mainActivity.getToken(),
                            user.getUserName(),
                            userfirstname.getText().toString(),
                            userlastname.getText().toString(),
                            useremail.getText().toString()
                    );
                }

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    user = response.body();
                }
            });
        } else {
            Toast.makeText(getContext(), "Impossible de modifier un autre utilisateur", Toast.LENGTH_LONG).show();
        }
    }

}


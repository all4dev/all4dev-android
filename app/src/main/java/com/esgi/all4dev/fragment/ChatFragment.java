package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.MembersAdapter;
import com.esgi.all4dev.adapter.MessagesAdapter;
import com.esgi.all4dev.object.Message;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.ChatConstant;
import com.esgi.all4dev.util.ChatUtils;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * Created by tayeb on 07/07/2016.
 */
public class ChatFragment extends Fragment {

    private WebSocketConnection mConnection = new WebSocketConnection();

    private static final String TAG = ChatFragment.class.getSimpleName();

    private ArrayList<Message> messages = new ArrayList<Message>();

    private User user;
    MainActivity mainActivity;

    private MessagesAdapter adapter;

    @BindView(R.id.editMessage)
    EditText editMessage;

    @BindView(R.id.btnSendMessage)
    Button btnSendMessage;

    @BindView(R.id.recyclerView_messages)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, rootView);

        mainActivity = (MainActivity) getActivity();
        user = mainActivity.getUser();

        recyclerView.setHasFixedSize(true);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new MessagesAdapter(messages, getContext());
        recyclerView.setAdapter(adapter);


        try {

            mConnection.connect(ChatConstant.URL, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "Connexion réussi à : " + ChatConstant.URL);
                    Log.d(TAG, "Bonjour : " + user.getUserName());

                }

                @Override
                public void onTextMessage(String payload) {
                    Log.d(TAG, " " + payload);

                    messages.add(ChatUtils.jsonToMessage(payload));
                    adapter.notifyDataSetChanged();

                    scrollToBottom();
                }

                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "Connexion perdu");
                }
            });

        } catch (WebSocketException e) {
            e.printStackTrace();
        }

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                /*
                 * On vérifie que notre edittext n'est pas vide
                 */
                if (!TextUtils.isEmpty(getMessage())) {

                    // On met "true" car c'est notre message
                    Message message = new Message(user.getUserName(), getMessage(), true);

                    String json = ChatUtils.messageToJson(message);

                    // On envoie notre message
                    mConnection.sendTextMessage(json);

                    // On ajoute notre message à notre list
                    messages.add(message);

                    // On notifie notre adapter
                    adapter.notifyDataSetChanged();

                    scrollToBottom();

                    // On efface !
                    editMessage.setText("");
                }
            }
                catch(Exception e) {
                    Toast.makeText(getContext(), "Vérifier votre serveur", Toast.LENGTH_LONG).show();
                }

            }
        });

        return rootView;
    }

    /**
     * Scroller notre recyclerView en bas
     */
    private void scrollToBottom() {
        recyclerView.scrollToPosition(messages.size() - 1);
    }

    private String getMessage() {
        return editMessage.getText().toString().trim();
    }
}

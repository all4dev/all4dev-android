package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.TaskAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Comment;
import com.esgi.all4dev.object.Commit;
import com.esgi.all4dev.object.DateTimeable;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;


public class CommentAddFragment extends Fragment {

    @BindView(R.id.fragment_add_text_comment)
    TextView taskTextComment;

    MainActivity mainActivity;

    Comment addedComment;
    private Project project;
    private String authToken;

    boolean canEdit = false;

    public static CommentAddFragment newInstance() {
        Bundle args = new Bundle();
        CommentAddFragment fragment = new CommentAddFragment();
        //args.putParcelable(Util.REFERENCE.USER, user);
        //fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_comment, container, false);
        ButterKnife.bind(this, rootView);
        mainActivity = (MainActivity) getActivity();

        authToken = mainActivity.getToken();
        project = mainActivity.getProject();

        return rootView;
    }


    @OnClick(R.id.fragment_add_comment_btn_save)
    public void save() {
        if (!canEdit) {
            new ApiManager(getActivity()) {

                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess() {
                    Toast.makeText(getActivity(), "Enregistrement effectué avec succeès", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(AppException e) {
                    e.toast(getActivity());
                    Util.log("fail");

                }

                @Override
                public void always() {
                }
            }.execute(new ApiRequest<Comment>() {
                @Override
                public Call<Comment> call(ApiService service) {
                    return service.addTaskComment(authToken, project.getLeaderUsername(), project.getName(), 1, taskTextComment.getText().toString());
                }

                @Override
                public void onResponse(Call<Comment> call, Response<Comment> response) {
                    addedComment = response.body();
                }
            });
        } else {
            Toast.makeText(getContext(), "Impossible de modifier un autre utilisateur", Toast.LENGTH_LONG).show();
        }
    }

}


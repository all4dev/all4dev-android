package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.TaskAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Comment;
import com.esgi.all4dev.object.Commit;
import com.esgi.all4dev.object.DateTimeable;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.Task;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by tayeb on 21/06/2016.
 */
public class TaskFragment extends Fragment {

    private TaskAdapter adapter;

    ArrayList<DateTimeable> dateTimeables = new ArrayList();
    private List<Comment> comments;
    private List<Commit> commits;
    MainActivity mainActivity;
    Project project;
    Task task;
    private String authToken;

    @BindView(R.id.fragment_commit_swipe_container)
    SwipeRefreshLayout swipe;

    @BindView(R.id.fragment_commit_list)
    ListView commitList;

    public static TaskFragment newInstance(Task task) {
        Bundle args = new Bundle();
        TaskFragment fragment = new TaskFragment();
        args.putParcelable(Util.REFERENCE.TASK, task);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tasksViews = inflater.inflate(R.layout.fragment_commit, container, false);
        ButterKnife.bind(this, tasksViews);

        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        project = mainActivity.getProject();
        task = getArguments().getParcelable(Util.REFERENCE.TASK);

        pull();

        adapter = new TaskAdapter(getContext(), dateTimeables);
        commitList.setAdapter(adapter);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pull();
            }
        });
        return tasksViews;
    }

    public void pull() {
        new ApiManager(getActivity()) {

            @Override
            public void onStart() {
                swipe.setRefreshing(true);
            }

            @Override
            public void onSuccess() {
                dateTimeables.clear();
                dateTimeables.addAll(comments);
                dateTimeables.addAll(commits);

                Collections.sort(dateTimeables, new Comparator<DateTimeable>() {
                    @Override
                    public int compare(DateTimeable lhs, DateTimeable rhs) {
                        return (lhs.getDateTime().getMillis() > rhs.getDateTime().getMillis()) ? 1 : -1;
                    }
                });
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {
                swipe.setRefreshing(false);
            }
        }.execute(
                new ApiRequest<List<Commit>>() {

                    @Override
                    public Call<List<Commit>> call(ApiService service) {
                        return service.getCommits(authToken, project.getLeaderUsername(), project.getName(), task.getId());
                    }

                    @Override
                    public void onResponse(Call<List<Commit>> call, Response<List<Commit>> response) {
                        commits = response.body();
                    }
                }, new ApiRequest<List<Comment>>() {

                    @Override
                    public Call<List<Comment>> call(ApiService service) {
                        return service.getComments(authToken, project.getLeaderUsername(), project.getName(), task.getId());
                    }

                    @Override
                    public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                        comments = response.body();
                    }
                });
    }

    @OnClick(R.id.fragment_commit_add_comment)
    public void click() {
        mainActivity.popFragment("Ajout commentaire", CommentAddFragment.newInstance());
    }
}

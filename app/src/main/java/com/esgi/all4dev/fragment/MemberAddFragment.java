package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.UsersAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class MemberAddFragment extends Fragment {

    @BindView(R.id.fragment_add_member_text_search)
    EditText textSearch;

    @BindView(R.id.fragment_add_member_recycler_result)
    RecyclerView recyclerResult;

    @BindView(R.id.fragment_add_comment_spinner_access)
    Spinner spinnerAccess;


    List<User> results;

    UsersAdapter adapter;
    private User user;
    MainActivity mainActivity;
    Project project;
    private String authToken;

    private Member newMember;


    public static MemberAddFragment newInstance() {
        Bundle args = new Bundle();
        MemberAddFragment fragment = new MemberAddFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Views = inflater.inflate(R.layout.fragment_add_member, container, false);
        ButterKnife.bind(this, Views);

        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        project = mainActivity.getProject();

        recyclerResult.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UsersAdapter(new UsersAdapter.ClickListener() {
            @Override
            public void onClick(int position) {
                user = results.get(position);
            }
        }, getContext());
        recyclerResult.setAdapter(adapter);


        spinnerAccess.setAdapter(new ArrayAdapter<Member.Access>(getContext(), android.R.layout.simple_spinner_item, Member.Access.values()));

        return Views;
    }


    @OnClick(R.id.fragment_add_member_btn_search)
    public void onSearch() {
        new ApiManager(getActivity()) {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess() {
                adapter.setUsers(results);
            }

            @Override
            public void onError(AppException e) {

            }

            @Override
            public void always() {
            }
        }.execute(new ApiRequest<List<User>>() {
            @Override
            public Call<List<User>> call(ApiService service) {
                String username = null;
                String email = null;
                String input = textSearch.getText().toString();
                if (Util.isValidEmail(input))
                    email = input;
                else
                    username = input;

                return service.getUsers(authToken, username, email);
            }

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                results = response.body();
                results.removeAll(project.getMembers());
            }
        });
    }

    @OnClick(R.id.fragment_add_comment_btn_validate)
    public void addMember() {
        if (user == null) {
            Toast.makeText(getContext(), "Sélectionnez un membre", Toast.LENGTH_SHORT).show();
            return;
        }
        new ApiManager(getActivity()) {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess() {
                Toast.makeText(getActivity(), "Ajout de " + user.getUserName() + " au projet" + project.getName() + " a été effectué avec succeès", Toast.LENGTH_SHORT).show();
                project.getMembers().add(newMember);
                getActivity().onBackPressed();
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {
                user = null;
                newMember = null;
            }
        }.execute(
                new ApiRequest<Member>() {

                    @Override
                    public Call<Member> call(ApiService service) {
                        return service.addMember(authToken,
                                project.getLeaderUsername(),
                                project.getName(),
                                user.getUserName(),
                                Member.Access.values()[spinnerAccess.getSelectedItemPosition()].name());
                    }

                    @Override
                    public void onResponse(Call<Member> call, Response<Member> response) {
                        newMember = response.body();
                    }
                });
    }
}

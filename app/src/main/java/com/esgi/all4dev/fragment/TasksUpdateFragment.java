package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Task;
import com.esgi.all4dev.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tayeb on 18/06/2016.
 */
public class TasksUpdateFragment extends Fragment {

    MainActivity mainActivity;

    @BindView(R.id.fragment_update_task_titre)
    TextView taskTitle;

    @BindView(R.id.fragment_update_task_desc)
    TextView taskDesc;

    @BindView(R.id.fragment_update_task_date)
    TextView taskDate;

    Task task;

    public static TasksUpdateFragment newInstance(Task task) {
        Bundle args = new Bundle();
        TasksUpdateFragment fragment = new TasksUpdateFragment();
        args.putParcelable(Util.REFERENCE.TASK, task);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_update_task, container, false);
        ButterKnife.bind(this, rootView);
        mainActivity = (MainActivity) getActivity();
        task = getArguments().getParcelable(Util.REFERENCE.TASK);
        taskTitle.setText(task.getName());
        taskDesc.setText(task.getBody());
        taskDate.setText(task.getLastUpdate().toString());

        return rootView;
    }
}


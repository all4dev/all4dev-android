package com.esgi.all4dev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.MembersAdapter;
import com.esgi.all4dev.adapter.ProjectsAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Member;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.object.Version;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class ProjectFragment extends Fragment implements ProjectsAdapter.ProjectClickListener, MembersAdapter.MemberClickListener {

    @BindView(R.id.fragment_project_titre)
    TextView fragmentProjectTitre;

    @BindView(R.id.fragment_project_description)
    TextView fragmentProjectDescription;

    @BindView(R.id.fragment_project_version)
    TextView fragmentProjectVersion;

    @BindView(R.id.fragment_project_swipe_container)
    SwipeRefreshLayout swipe;

    @BindView(R.id.fragment_project_list_members)
    RecyclerView fragment_project_recycler_view_members;

    MainActivity mainActivity;

    private User user;
    private MembersAdapter adapter;
    private Project project;
    private String authToken;
    private Version version;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_project, container, false);
        ButterKnife.bind(this, rootView);
        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        user = mainActivity.getUser();

        //fragment_project_recycler_view_members.setHasFixedSize(true);
        //fragment_project_recycler_view_members.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        fragment_project_recycler_view_members.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new MembersAdapter(this, getContext());
        fragment_project_recycler_view_members.setAdapter(adapter);

        project = mainActivity.getProject();
        fragmentProjectTitre.setText(project.getName());
        fragmentProjectDescription.setText(project.getDescription());
        fragmentProjectVersion.setText(project.getCurrentVersion());

        updateListMember();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullMembers();
            }
        });

        //Rattache le swipe de la classe MemberTouch au recycler view
        ItemTouchHelper.Callback callback = new MemberTouch(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(fragment_project_recycler_view_members);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateListMember();
    }

    private void updateListMember() {
        adapter.setMembers(project.getMembers());
    }


    public void pullMembers() {
        new ApiManager(getActivity()) {
            @Override
            public void onStart() {
                swipe.setRefreshing(true);
            }

            @Override
            public void onSuccess() {
                updateListMember();
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {
                swipe.setRefreshing(false);

            }
        }.execute(new ApiRequest<List<Member>>() {
            @Override
            public Call<List<Member>> call(ApiService service) {
                return service.getProjectsMembers(authToken, project.getLeaderUsername(), project.getName());
            }

            @Override
            public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                project.setMembers(response.body());
            }
        });
    }

    public void deleteMember(final Member member) {
        new ApiManager(getActivity()) {
            @Override
            public void onStart() {
                swipe.setRefreshing(true);
            }

            @Override
            public void onSuccess() {
                project.getMembers().remove(member);
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {
                swipe.setRefreshing(false);

            }
        }.execute(new ApiRequest<Void>() {
            @Override
            public Call<Void> call(ApiService service) {
                return service.deleteProjectMember(authToken, project.getLeaderUsername(), project.getName(), member.getUserName());
            }

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }
        });
    }

    @OnClick(R.id.fragment_project_add_member)
    public void click () {
        mainActivity.popFragment("Ajout membre", MemberAddFragment.newInstance());
    }

    @Override
    public void onClick(int position) {
        Member member = project.getMembers().get(position);
        mainActivity.popFragment(member.getUserName(), ProfileFragment.newInstance(member));
    }



    //Class qui permet de supprimer un élément lors du swipe
    public class MemberTouch extends ItemTouchHelper.SimpleCallback {
        private MembersAdapter membersAdapter;

        public MemberTouch(MembersAdapter membersAdapter){
            super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            this.membersAdapter = membersAdapter;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            //TODO: Not implemented here
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int index = viewHolder.getAdapterPosition();
            deleteMember(project.getMembers().get(index));
            membersAdapter.remove(index);
        }
    }
}

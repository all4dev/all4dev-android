package com.esgi.all4dev.fragment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.adapter.CustomCalendarAdapter;
import com.esgi.all4dev.api.ApiManager;
import com.esgi.all4dev.api.ApiRequest;
import com.esgi.all4dev.api.ApiService;
import com.esgi.all4dev.api.exception.AppException;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.Task;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;
import com.telerik.android.common.Function;
import com.telerik.widget.calendar.CalendarAdapter;
import com.telerik.widget.calendar.CalendarDayCell;
import com.telerik.widget.calendar.CalendarElement;
import com.telerik.widget.calendar.CalendarSelectionMode;
import com.telerik.widget.calendar.CalendarStyles;
import com.telerik.widget.calendar.RadCalendarView;
import com.telerik.widget.calendar.decorations.CircularCellDecorator;
import com.telerik.widget.calendar.events.Event;
import com.telerik.widget.calendar.events.EventRenderer;
import com.telerik.widget.calendar.events.EventsDisplayMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by tayeb on 02/06/2016.
 */
public class CalendarFragment extends Fragment{

    private String authToken;
    private Project project;
    private User user;
    private List<Task> tasks;

    RadCalendarView calendarView = null;

    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_calendar, container, false);
        ButterKnife.bind(this, root);

        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        project = mainActivity.getProject();
        user = mainActivity.getUser();

         calendarView = (RadCalendarView) root.findViewById(R.id.calendarView);


        //Couleur grise pour les week-end
        Function<Long, Integer> customColorFunction = new Function<Long, Integer>() {
            private Calendar calendar = Calendar.getInstance();
            private int day;

            @Override
            public Integer apply(Long argument) {
                calendar.setTimeInMillis(argument);
                day = calendar.get(Calendar.DAY_OF_WEEK);
                if (day == Calendar.SATURDAY || day == Calendar.SUNDAY)
                    return Color.GRAY;

                return null;
            }
        };

        calendarView.setDateToColor(customColorFunction);
        calendarView.setDayNameToColor(customColorFunction);

        calendarView.setAdapter(new CustomCalendarAdapter(calendarView));
        calendarView.getAdapter().setStyle(CalendarStyles.materialDark(getActivity()));
        calendarView.setBackgroundColor(Color.TRANSPARENT);

        // Type Affichage
        calendarView.setSelectionMode(CalendarSelectionMode.Single);
        calendarView.setEventsDisplayMode(EventsDisplayMode.Inline);

        // Customizations
        CalendarAdapter adapter = calendarView.getAdapter();
        adapter.setDateCellBackgroundColor(Color.TRANSPARENT, Color.TRANSPARENT);
        adapter.setTodayCellBackgroundColor(Color.TRANSPARENT);
        adapter.setSelectedCellBackgroundColor(Color.TRANSPARENT);
        adapter.setDayNameBackgroundColor(Color.TRANSPARENT);
        adapter.setTitleBackgroundColor(Color.TRANSPARENT);

        //Couleur pour le titre, et date
        adapter.setTitleTextColor(Color.parseColor("#391033"));
        adapter.setDayNameTextColor(Color.parseColor("#A75065"));
        adapter.setDateTextColor(Color.parseColor("#391033"), Color.parseColor("#A85066"));
        adapter.setTodayCellTextColor(Color.BLACK);
        adapter.setTodayCellTypeFace(Typeface.create("sans-serif", Typeface.BOLD));
        adapter.setSelectedCellTextColor(Color.WHITE);
        adapter.setTitleTextSize(com.telerik.android.common.Util.getDimen(TypedValue.COMPLEX_UNIT_SP, 38));
        adapter.setTitleTypeFace(Typeface.create("sans-serif-light", Typeface.NORMAL));
        adapter.setTitleTextPosition(CalendarElement.LEFT | CalendarElement.CENTER_VERTICAL);
        adapter.setTodayCellBorderColor(Color.TRANSPARENT);

        //Cercle du point lors du titre centrer
        adapter.setDateTextPosition(CalendarElement.CENTER);
        adapter.setDayNameTextPosition(CalendarElement.CENTER);

        //Couleur de la ligne de l'event
        adapter.setInlineEventsBackgroundColor(Color.parseColor("#391033"));

        //Cacher les ligne du calendrier
        calendarView.setHorizontalScroll(true);
        calendarView.setDrawingVerticalGridLines(false);
        calendarView.getGridLinesLayer().setColor(Color.parseColor("#E16F85"));
        calendarView.setTitleHeight((int) com.telerik.android.common.Util.getDimen(TypedValue.COMPLEX_UNIT_DIP, 100));
        calendarView.title().setPaddingHorizontal((int) com.telerik.android.common.Util.getDimen(TypedValue.COMPLEX_UNIT_DIP, 10));

        //Decoration : Affichage d'un Cercle lors du click
        CircularCellDecorator decorator = new CircularCellDecorator(calendarView);
        decorator.setStroked(false);
        decorator.setColor(Color.parseColor("#391033"));
        decorator.setScale(.7F);
        calendarView.setCellDecorator(decorator);

        //Cacher le titre de la tache
        calendarView.getEventAdapter().setRenderer(new CustomEventRenderer(getActivity()));


        calendarView.getGestureManager().setDoubleTapToChangeDisplayMode(false);
        calendarView.getGestureManager().setPinchOpenToChangeDisplayMode(false);
        calendarView.getGestureManager().setPinchCloseToChangeDisplayMode(false);

        pullTasks();

        return root;
    }

    private int funColor = Color.parseColor("#47C2B4");




    //Change la couleur de l'écriture du nom de la tache en Blanc
    public void updateEventsColors(List<Event> events, int color, int allDayColor) {
        for(Event e : events) {
            if(e.isAllDay()) {
                e.setEventColor(allDayColor);
            } else {
                e.setEventColor(color);
            }
        }
    }

    @OnClick(R.id.fragment_calendar_add_event)
    public void click () {
        mainActivity.popFragment("Nouvel évenement", TasksAddFragment.newInstance());
    }


    public List<Event> setTasks(List<Task> tasks) {
        final Calendar calendar = Calendar.getInstance();
        long eventStart = calendar.getTimeInMillis();
        calendar.add(Calendar.HOUR, 1);
        long eventEnd = calendar.getTimeInMillis();

        List<Event> events = new ArrayList<Event>();

        for (Task task : tasks) {
            events.add(new Event(task.getName(), task.getLastUpdate().getMillis(), task.getLastUpdate().getMillis()));
            System.out.println(task.getLastUpdate().toString());
        }

        this.updateEventsColors(events, Color.WHITE, Color.parseColor("#FAABB9"));

        calendarView.getEventAdapter().setEvents(events);
        calendarView.notifyDataChanged();

        return events;
    }

    public void pullTasks() {
        new ApiManager(getActivity()) {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess() {
                for (int i = 0; i < tasks.size(); i++) {
                    Task task = tasks.get(i);
                    List<Event> events = setTasks(tasks);
                    Util.log(task.getAuthorName() + " " + getStatus() + " " + task.getName() + " " );
                }
            }

            @Override
            public void onError(AppException e) {
                e.toast(getActivity());
            }

            @Override
            public void always() {

            }
        }.execute(
                new ApiRequest<List<Task>>() {

                    @Override
                    public Call<List<Task>> call(ApiService service) {
                        return service.getTasks(authToken, project.getLeaderUsername(), project.getName());
                    }

                    @Override
                    public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                        tasks = response.body();
                    }
                });
    }

    class CustomEventRenderer extends EventRenderer {

        public CustomEventRenderer(Context context) {
            super(context);
        }

        @Override
        public void renderEvents(Canvas canvas, CalendarDayCell cell) {
            //super.renderEvents(canvas, cell); Prevent rendering.
        }
    }
}


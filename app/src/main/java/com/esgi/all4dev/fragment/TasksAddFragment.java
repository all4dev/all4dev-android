package com.esgi.all4dev.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.esgi.all4dev.MainActivity;
import com.esgi.all4dev.R;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.Task;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tayeb on 18/06/2016.
 */
public class TasksAddFragment extends Fragment implements View.OnClickListener {

    MainActivity mainActivity;
    Project project;

    private TextView text_date;
    private DatePicker date_picker;
    private Button button;

    private List<Task> tasks;
    private String authToken;

    @BindView(R.id.fragment_add_task_btn_date)
    Button btnDatePicker;

    @BindView(R.id.fragment_add_task_btn_time_start)
    Button btnTimePickerStart;

    @BindView(R.id.fragment_add_task_date)
    EditText txtDate;

    @BindView(R.id.fragment_add_task_time_start)
    EditText txtTimeStart;

    @BindView(R.id.fragment_add_task_btn_time_end)
    Button btnTimePickerEnd;

    @BindView(R.id.fragment_add_task_time_end)
    EditText txtTimeEnd;

    private int mYear, mMonth, mDay, mHour, mMinute;


    public static TasksAddFragment newInstance() {
        Bundle args = new Bundle();
        TasksAddFragment fragment = new TasksAddFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View Views = inflater.inflate(R.layout.fragment_add_task, container, false);
        ButterKnife.bind(this, Views);

        mainActivity = (MainActivity) getActivity();
        authToken = mainActivity.getToken();
        project = mainActivity.getProject();

        btnDatePicker.setOnClickListener(this);
        btnTimePickerStart.setOnClickListener(this);
        btnTimePickerEnd.setOnClickListener(this);

        return Views;
    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePickerStart) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTimeStart.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }

        if (v == btnTimePickerEnd) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTimeEnd.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
}


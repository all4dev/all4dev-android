package com.esgi.all4dev;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.esgi.all4dev.adapter.MenuAdapter;
import com.esgi.all4dev.fragment.CalendarFragment;
import com.esgi.all4dev.fragment.ChatFragment;
import com.esgi.all4dev.fragment.ProfileFragment;
import com.esgi.all4dev.fragment.ProjectFragment;
import com.esgi.all4dev.fragment.TasksFragment;
import com.esgi.all4dev.menu.GroupMenuItem;
import com.esgi.all4dev.menu.MenuItem;
import com.esgi.all4dev.object.Project;
import com.esgi.all4dev.object.User;
import com.esgi.all4dev.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.menu_drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.menu_drawer_expandable_menu)
    ExpandableListView expandableMenu;

    @BindView(R.id.menu_drawer_my_account)
    ImageButton myAccount;

    @BindView(R.id.main_activity_toolbar)
    Toolbar toolbar;

    ActionBarDrawerToggle toggle;

    private String authToken;
    private User user;
    private Project project;
    public CalendarFragment calendarFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Recuperation du projet cliqué
        Intent intent = getIntent();
        authToken = intent.getStringExtra(Util.REFERENCE.AUTH_TOKEN);
        user = intent.getParcelableExtra(Util.REFERENCE.USER);
        project = intent.getParcelableExtra(Util.REFERENCE.PROJECT);


        setSupportActionBar(toolbar);


        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        calendarFragment = new CalendarFragment();

        final List<GroupMenuItem> menuItems = new ArrayList<GroupMenuItem>();

        GroupMenuItem projectGroup = new GroupMenuItem(0, "Project", new ProjectFragment());
        projectGroup.addItem(new MenuItem(0, "Calendar", calendarFragment));

        menuItems.add(projectGroup);


        GroupMenuItem taskGroup = new GroupMenuItem(0, "Tasks", new TasksFragment());

        menuItems.add(taskGroup);

        GroupMenuItem chatGroup = new GroupMenuItem(0, "Chat", new ChatFragment());

        menuItems.add(chatGroup);

        MenuAdapter menuAdapter = new MenuAdapter(getApplicationContext(), menuItems);
        expandableMenu.setGroupIndicator(null);
        expandableMenu.setChildIndicator(null);
        expandableMenu.setDividerHeight(0);
        expandableMenu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        expandableMenu.setAdapter(menuAdapter);

        for(int i =0; i < menuItems.size(); i++) {
            expandableMenu.expandGroup(i, false);
        }

        expandableMenu.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                System.out.println("group click" + groupPosition);
                onClickMenu(menuItems.get(groupPosition));
                return true;
            }
        });
        expandableMenu.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                System.out.println("child click  " + groupPosition + "   " + childPosition);
                onClickMenu(menuItems.get(groupPosition).getItems().get(childPosition));
                return false;
            }
        });
        onClickMenu(projectGroup);
    }

    public void onClickMenu(MenuItem item) {
        if (item.isSelectable()) {
            loadContent(item.getName(), item.getFragment());
        }
    }

    public void loadContent(String name, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(R.id.main_activity_frame_content, fragment)
                .commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toggle.setDrawerIndicatorEnabled(true);

        setTitle(name);
        drawer.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.menu_drawer_my_account)
    public void profile() {
        loadContent("Mon compte", ProfileFragment.newInstance(getUser()));
    }

    public String getToken() {
        return authToken;
    }

    public Project getProject() {
        return project;
    }

    public User getUser() {
        return user;
    }

    public void popFragment(String name, Fragment fragment) {
        setTitle(name);
        drawer.closeDrawer(Gravity.LEFT);
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_activity_frame_content, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                toggle.setDrawerIndicatorEnabled(true);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
